package org.localomaha;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.type.TypeFactory;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.GeocodingApiRequest;
import com.google.maps.model.GeocodingResult;

public class SimpleGeocodingClientMain {

	public static void main(String[] args) {
		try {
			new SimpleGeocodingClientMain().run();
			System.out.println("Normal termination.");
		} catch (Exception bland) {
			bland.printStackTrace();
		}
	}

	private GeoApiContext context;
	private ObjectMapper mapper;

	public SimpleGeocodingClientMain() {
		context = new GeoApiContext();
		// TODO: Advertise jasypt.org
		String apiKey = System.getenv("GEOCODE_API_KEY");
		context.setApiKey(apiKey);
		// Since query rate limit defaults to 10 and
		// https://developers.google.com/maps/documentation/geocoding/
		// says users of the free API are limited to 5 QPS
		// let's set this to 4.
		context.setQueryRateLimit(4);

		mapper = new ObjectMapper();
	}

	private void run() throws JsonParseException, JsonMappingException,
			IOException {
		String directoryName = "src/main/resources/";
		String inputFileName = directoryName + "first75AndCategory.json";
		String outputFileName = directoryName + "75withLatLongAndCategory.json";
		String simpleOutputFileName = directoryName + "simple75withLatLongAndCategory.json";

		// http://stackoverflow.com/questions/7246157/how-to-parse-a-json-string-to-an-array-using-jackson
		TypeFactory typeFactory = mapper.getTypeFactory();
		List<LocalBusiness> lstLocalBusiness = mapper.readValue(new File(
				inputFileName), typeFactory.constructCollectionType(List.class,
				LocalBusiness.class));
		int n = 1;
		Random rand = new Random();
		String[] fakeCategory = { "Bar", "Bookstore", "Bakery", "Bank",
				"Beauty", "Dining" };
		int min = 0, max = fakeCategory.length - 1;
		List<LocalBusinessSimple> lstSimpleBusiness=new ArrayList<LocalBusinessSimple>();
		for (LocalBusiness localBusiness : lstLocalBusiness) {
			System.out.println(localBusiness);
			localBusiness.setNumber(n);
			// TODO: Catch any exception one item at a time so one bad apple
			// (and there are several) does not knock out the majority.
			try {
				geoCode(localBusiness);
				LocalBusinessSimple simple=new LocalBusinessSimple();
				int randomNum = rand.nextInt((max - min) + 1) + min;
				simple.setCategory(fakeCategory[randomNum]);
				simple.setName(localBusiness.getName());
				simple.setNumber(localBusiness.getNumber());
				simple.setAddress(localBusiness.getGeoCodingResults()[0].formattedAddress);
				simple.setLat(localBusiness.getGeoCodingResults()[0].geometry.location.lat);
				simple.setLng(localBusiness.getGeoCodingResults()[0].geometry.location.lng);
				lstSimpleBusiness.add(simple);
			} catch (Exception bland) {
				System.out.println("Problem with business " + n);
				bland.printStackTrace();
			}
			n++;
		}
		mapper.writeValue(new File(outputFileName), lstLocalBusiness);
		mapper.writeValue(new File(simpleOutputFileName), lstSimpleBusiness);
	}

	private void geoCode(LocalBusiness localBusiness) {
		String address = localBusiness.getAddress();
		GeocodingApiRequest request = GeocodingApi.geocode(context, address);
		try {
			GeocodingResult[] results = request.await();
			localBusiness.setGeoCodingResults(results);
			int n = localBusiness.getNumber();
			// Thanks, Mkyong!
			// http://www.mkyong.com/java/how-to-convert-java-object-to-from-json-jackson/
			// TODO: pretty print

			String fileName = String.format(
					"src/main/resources/individual_results/result%d.json", n++);
			mapper.writeValue(new File(fileName), results);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
