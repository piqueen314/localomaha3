'use strict';

angular.module('localOmaha').factory('localOmahaResource', function($resource) {
    var data = $resource('/app/data/simple75withLatLong_formatted.json');

    function getAllLocalOmahaBusinesses() {
        return data.query();
    }

    return {
        getAllLocalOmahaBusinesses:getAllLocalOmahaBusinesses
    }
});