package org.localomaha;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;

import static org.junit.Assert.*;

import org.junit.Test;

public class HelloDom4j {

	@Test
	public void test() throws MalformedURLException, DocumentException {
		// tidy < local2.html > local3.html
		// then delete the <head> section out using vim
		// tidy < local3.html > local4.html
		// tidy -asxml -i < local4.html > local5.html
		File htmlFile = new File("src/main/resources/local5.html");
		assertTrue(htmlFile.exists());
		Document document = parse(htmlFile.toURI().toURL());
		assertNotNull(document);
	}

	
	
	
	
	public Document parse(URL url) throws DocumentException {
        SAXReader reader = new SAXReader();
        Document document = reader.read(url);
        return document;
    }
	
}
