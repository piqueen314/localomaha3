#!/usr/bin/env ruby
#$LOAD_PATH.unshift(File.expand_path(File.dirname(__FILE__)))

require 'nokogiri'
require 'open-uri'
require_relative 'store' # Many thanks Carl Z!

DIRECTORY='http://shoplocalomaha.com/Directory'

lstStores=[]
dir_html = Nokogiri::HTML.parse(open(DIRECTORY))
id=1
dir_html.css('a.open_profile_page').each do |merchant_link|
  profile_url=merchant_link.attr('href')
  profile_html=Nokogiri::HTML.parse(open(profile_url))
  name=profile_html.css('h2.title').text
  category=profile_html.css('p.category_name').text
  address=profile_html.css('p.address').text
  phone=profile_html.css('p.phone').text
  facebook=profile_html.css('a.facebook')
  #facebook=facebook.attr('href')  if !facebook.nil?

  storeUrl=profile_html.css('p.site')
  storeUrl=storeUrl.text if !storeUrl.nil?

  lstStores << Store.new(id,name,profile_url, category,address,phone,facebook,storeUrl)
  if 0==(id%10)
    puts "Parsed #{id} so far..."
  end
  id=id+1
end


output = File.open("stores.json", "w")
output <<  lstStores.to_json
output.close

puts "Normal termination."