
require 'jsonable'  # https://github.com/treeder/jsonable
require_relative 'general_init'

class Store
  include Jsonable
  include GeneralInit

  attr_reader :name

  def initialize(id,name,url,category,address,phone,facebook,storeUrl)
    @id=id
    @name=name
    @url=url
    @category=category
    @address=address
    @phone = phone
    @facebook=facebook
    @storeUrl=storeUrl
  end
end

