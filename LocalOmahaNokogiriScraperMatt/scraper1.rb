#!/usr/bin/env ruby
$LOAD_PATH.unshift(File.expand_path(File.dirname(__FILE__)))

require 'nokogiri'
require 'open-uri'
require 'store'

DIRECTORY='http://shoplocalomaha.com/Directory'

def extract_details(profile_url)
    profile_html=Nokogiri::HTML.parse(open(profile_url))
    puts("\t"+profile_html.css('p.category_name').text)
end


dir_html = Nokogiri::HTML.parse(open(DIRECTORY))
dir_html.css('a.open_profile_page').each do |merchant_link|
  profile_url=merchant_link.attr('href')
  puts profile_url
  extract_details(profile_url)
end

